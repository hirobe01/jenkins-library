def call(Map config = [:]) {
    cleanWs()

    unstash 'qualification_tests'

    dir('Tests/qualification_tests') {
        loadWindowsScript(name: 'checkHitestRunning.bat')
        bat 'checkHitestRunning.bat'

        loadWindowsScript(name: 'checkUvisionRunning.bat')
        bat 'checkUvisionRunning.bat'

        loadWindowsScript(name: 'setupPython.bat')
        bat 'setupPython.bat'

        // compare test bench config to requirements/environment
        loadPythonScript(name: 'conf2prop.py')
        bat '.venv\\Scripts\\python.exe conf2prop.py -c C:/testbench/config.json -o properties.txt -e PCBA_ID -e PROJECT_NAME'
        // set properties file as env
        script {
            def props = readProperties file: 'properties.txt'
            env.PCBA_SN = props.PCBA_SN
            env.POWER_SUPPLY_NAME = props.POWER_SUPPLY_NAME
            env.POWER_SUPPLY_SN = props.POWER_SUPPLY_SN
            env.POWER_SUPPLY_PORT = props.POWER_SUPPLY_PORT
            env.POWER_SUPPLY_CHANNEL = props.POWER_SUPPLY_CHANNEL
            env.SETUP_PLACE = props.SETUP_PLACE
            env.KEIL_PATH = props.KEIL_PATH
            env.KEIL_VERSION = props.KEIL_VERSION
            env.CUSBC_PATH = props.CUSBC_PATH
            env.CUSBC_PORT = props.CUSBC_PORT
            env.USB_PORT_JLINK = props.USB_PORT_JLINK
            env.USB_PORT_BABYLIN = props.USB_PORT_BABYLIN
            env.JLINK_PATH = props.JLINK_PATH
            env.JLINK_TARGET_DEVICE = props.JLINK_TARGET_DEVICE
        }

        // set USB power to j-link on before debugging
        bat '"%CUSBC_PATH%\\CUSBC.exe" /S:%CUSBC_PORT% 1:%USB_PORT_JLINK%'

        loadPythonScript(name: 'powerSupply.py')
        // switch power supply on before debugging
        bat '.venv\\Scripts\\python.exe powerSupply.py --on'

        // set TestEnable On
        bat '"%JLINK_PATH%\\JLink.exe" -device %JLINK_TARGET_DEVICE% -if JTAG -autoconnect 1 -CommandFile C:\\testbench\\Segger\\PowerOn.jlink'

        loadPythonScript(name: 'execKeilDebugTests.py')
        timeout(time: 8, unit: 'MINUTES') {
            bat '.venv\\Scripts\\python.exe execKeilDebugTests.py -t . -s %WORKSPACE%\\Source -c execKeilDebugTests.cfg'
        }

        // // switch power supply off after debugging
        // bat '.venv\\Scripts\\python.exe powerSupply.py --off '

        // // set USB Power to J-link Off after debugging
        // bat '"%CUSBC_PATH%\\CUSBC.exe" /S:%CUSBC_PORT% 0:%USB_PORT_JLINK%'

        loadPythonScript(name: 'keilDebugLog2JUnit.py')
        bat '.venv\\Scripts\\python.exe keilDebugLog2JUnit.py -t . -o Results_Debug.xml -c keilDebugLog2JUnit.cfg'

        // start HiTest section

        // flash compiled software to DUT
        bat '%KEIL_PATH%\\UV4\\UV4.exe -f %WORKSPACE%\\%KEIL_PROJECT_PATH% -t Basic -j0 -o %STAGE_FULL_PATH%\\ConsoleLog.txt'

        // print console lot to jenkins log
        bat 'type ConsoleLog.txt'

        // switch power supply off after flashing
        bat '.venv\\Scripts\\python.exe powerSupply.py --off'

        // set USB Power to J-link Off after flashing
        bat '"%CUSBC_PATH%\\CUSBC.exe" /S:%CUSBC_PORT% 0:%USB_PORT_JLINK%'

        loadWindowsScript(name: 'getHitest.bat')
        bat 'getHitest.bat 3.8.2'

        loadWindowsScript(name: 'getHitestPlugin.bat')
        bat 'getHitestPlugin.bat LIN'

        bat 'getHitestPlugin.bat Hameg'

        loadPythonScript(name: 'configHiTest.py')
        bat '.venv\\Scripts\\python.exe configHiTest.py'

        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
            timeout(time: 15, unit: 'MINUTES') {
                loadPythonScript(name: 'execHiTest.py')
                bat '.venv\\Scripts\\python.exe execHiTest.py'
            }
        }

        // switch power supply off after testing
        bat '.venv\\Scripts\\python.exe powerSupply.py --off'

        loadPythonScript(name: 'hiTestLog2csv.py')
        bat '.venv\\Scripts\\python.exe hiTestLog2csv.py -d HiTest/data/Log.db -c HiTestLog.csv'

        loadPythonScript(name: 'hiTest2JUnit.py')
        bat '.venv\\Scripts\\python.exe hiTest2JUnit.py -i **/Calc/Results.tdms -o Results_HiTest.xml'

        // merge jUnit results
        bat ".venv\\Scripts\\python.exe -m junit2htmlreport --merge Results.xml Results_Debug.xml Results_HiTest.xml"

        // generate html report
        bat ".venv\\Scripts\\python.exe -m junit2htmlreport Results.xml ${STAGE_NAME_FILE}_report.html"

    }
}
