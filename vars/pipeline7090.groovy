def call(Map config = [:]) {
    skip_static_resource_tests = config.skip_static_resource_tests
    skip_static_code_analysis = config.skip_static_code_analysis
    skip_dynamic_tests = config.skip_dynamic_tests
    skip_dynamic_resource_tests = config.skip_dynamic_resource_tests
    skip_qualification_tests = config.skip_qualification_tests
    skip_system_tests = config.skip_system_tests
    VC_Manage_Project = config.vc_manage_project

    pipeline {
        agent any
        options {
            disableConcurrentBuilds()

            office365ConnectorWebhooks([[
                name: 'Teams Connector',
                startNotification: true,
                notifyFailure: true,
                notifySuccess: true,
                notifyAborted: true,
                notifyUnstable: true,
                notifyBackToNormal: true,
                notifyRepeatedFailure: true,
                url: "https://hiliteglobal.webhook.office.com/webhookb2/b153175b-cc6d-46e5-926a-42ec14167501@b29a4d40-6afa-4189-87cf-96634d4c40b9/IncomingWebhook/041007f099a5468195a1a96d9f10d611/1e1de10d-e083-4c9b-8e54-3c48670a6787"
            ]])
        }
        stages {
            stage('Initializations'){
                steps {
                    script {
                        PROJECT_NAME = '7090'
                        println("PROJECT_NAME: ${PROJECT_NAME}")

                        PCBA_ID = '5000708269-E002'
                        println("PCBA_ID: ${PCBA_ID}")

                        SVN_BIN_PATH = '"C:\\Program Files\\TortoiseSVN\\bin\\"'
                        println("SVN_BIN_PATH: ${SVN_BIN_PATH}")

                        AXIVION_DASHBOARD_URL = "https://nt-subv01.eu.hilite-ind.net:9443/axivion/"
                        println("AXIVION_DASHBOARD_URL: ${AXIVION_DASHBOARD_URL}")
                    }

                    script {
                        println("BUILD_NUMBER: ${BUILD_NUMBER}")

                        println("BUILD_ID: ${BUILD_ID}")

                        println("BUILD_URL: ${BUILD_URL}")

                        println("BUILD_TAG: ${BUILD_TAG}")

                        SVN_REVISION = bat(script: "${SVN_BIN_PATH}svn info --show-item last-changed-revision", returnStdout: true).trim().readLines().drop(1).join(' ')
                        println("SVN_REVISION: ${SVN_REVISION}")
                        env.SVN_REVISION = "${SVN_REVISION}"

                        SVN_URL = bat(script: "${SVN_BIN_PATH}svn info --show-item url", returnStdout: true).trim().readLines().drop(1).join(' ')
                        println("SVN_URL: ${SVN_URL}")
                        SVN_REPOS_ROOT_URL = bat(script: "${SVN_BIN_PATH}svn info --show-item repos-root-url", returnStdout: true).trim().readLines().drop(1).join(' ')
                        println("SVN_REPOS_ROOT_URL: ${SVN_REPOS_ROOT_URL}")

                        println("BRANCH_NAME: ${BRANCH_NAME}")

                        BRANCH_NAME_REPL = BRANCH_NAME.replace('/', '.')
                        println("BRANCH_NAME_REPL: ${BRANCH_NAME_REPL}")

                        def now = new Date()
                        AXIVION_WORKSPACE_DATE = now.format("yyyy-MM-dd'T'HH:mm:ss'Z'", TimeZone.getTimeZone('Berlin/Europe'))
                        println("AXIVION_WORKSPACE_DATE: ${AXIVION_WORKSPACE_DATE}")

                        AXIVION_PROJECT_NAME = "${PROJECT_NAME}.${BRANCH_NAME_REPL}"
                        println("AXIVION_PROJECT_NAME: ${AXIVION_PROJECT_NAME}")

                        AXIVION_PROJECT_URL = "${AXIVION_DASHBOARD_URL}projects/${AXIVION_PROJECT_NAME}"
                        println("AXIVION_PROJECT_URL: ${AXIVION_PROJECT_URL}")

                        // get branch type
                        BRANCH_TYPE = ''
                        BRANCH_NAME_SPLITTED = BRANCH_NAME.split("/")
                        if (BRANCH_NAME_SPLITTED[0] == 'trunk') {
                            BRANCH_TYPE = 'trunk'
                        }
                        else if (BRANCH_NAME_SPLITTED.length > 1){
                            if (BRANCH_NAME_SPLITTED[1] == 'release') {
                                BRANCH_TYPE = 'release'
                            }
                            else if (BRANCH_NAME_SPLITTED[0] == 'tags') {
                                BRANCH_TYPE = 'tags'
                            }
                            else {
                                BRANCH_TYPE = BRANCH_NAME_SPLITTED[1]
                            }
                        }

                        println("BRANCH_TYPE: ${BRANCH_TYPE}")

                    }

                    buildDescription "SVN revision: <a href='${SVN_URL}/?p=${SVN_REVISION}'>${SVN_REVISION}</a>"
                }
            }
            stage('Build') {
                environment {
                    UVISION_BIN = "C:\\Keil_v537\\UV4\\UV4.exe"
                }

                steps{
                    script {
                        env.SW_VER = bat(script: "%WORKSPACE%\\Source\\set_software_version.bat", returnStdout: true).trim().readLines().drop(1).join(" ")
                    }
                    bat "%WORKSPACE%\\Source\\set_software_version.bat"

                    bat "%UVISION_BIN% -r %WORKSPACE%\\Source\\3-2.uvprojx -t Basic -j0 -o %WORKSPACE%\\ConsoleLog.txt"

                    zip zipFile: "flash_${env.SW_VER}.zip", archive: false, glob: 'Source/SW_ver.h,Source/flash.bat,Source/*.uvprojx,Source/*.uvoptx,Source/Objects/*.axf,Source/Objects/*.hex'

                    stash includes: 'Source/**, Tests/system_tests/**', name: 'system_tests', useDefaultExcludes: false

                    stash includes: 'Source/**, Tests/qualification_tests/**', name: 'qualification_tests', useDefaultExcludes: false

                    stash includes: 'Source/**, Tests/dynamic_resources/**', name: 'dynamic_resource_tests', useDefaultExcludes: false

                }
                post{
                    always{
                        bat "type %WORKSPACE%\\ConsoleLog.txt"

                        recordIssues healthy: 1, unhealthy: 0, publishAllIssues: true, skipBlames: true, skipPublishingChecks: true, enabledForFailure: true, tools: [clang(id: 'arm-warnings', name: 'Arm', pattern: 'ConsoleLog.txt')]

                        archiveArtifacts allowEmptyArchive: false, artifacts: 'Source/SW_ver.h, *.zip, Source/*.uvprojx, Source/Objects/*.axf, Source/Objects/*.hex, Source/Objects/*.htm, ConsoleLog.txt, Source/Listings/*.map', caseSensitive: true, defaultExcludes: true, fingerprint: true, onlyIfSuccessful: false
                    }
                }

            }
            stage('Tests') {
                parallel {
                    stage('Static resource tests') {
                        environment {
                            STAGE_PATH = 'Tests\\static_resources'
                            STAGE_FULL_PATH = "${WORKSPACE}\\${STAGE_PATH}"
                            STAGE_NAME_FILE = STAGE_NAME.toLowerCase().replace(' ','_')
                        }
                        when {
                            anyOf {
                                expression{
                                    skip_static_resource_tests == false
                                }
                                expression{
                                    BRANCH_TYPE == 'trunk'
                                }
                                expression{
                                    BRANCH_TYPE == 'release'
                                }
                                expression{
                                    BRANCH_TYPE == 'tags'
                                }
                            }
                        }
                        steps {
                             catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                                staticResourceTests()
                             }
                        }
                        post{
                            always{
                                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                                    postTest()
                                }
                            }
                        }
                    }
                    stage('Static Code Analysis') {
                        environment {
                            AXIVION_WORKSPACE_DATE = "${AXIVION_WORKSPACE_DATE}"
                            AXIVION_VERSION_NAME = "ID:${BUILD_ID} Rev:${SVN_REVISION}"
                            AXIVION_PROJECTNAME = "${AXIVION_PROJECT_NAME}"
                            REQUESTS_CA_BUNDLE = "C:\\Axivion\\dashboardserver\\cert\\hilite.cer"
                            PYTHONPATH = "${PYTHONPATH};C:\\Axivion\\bauhaus-suite\\lib\\scripts"
                            AXIVION_DATABASES_DIR = "C:\\Axivion\\dashboardserver"
                            AXIVION_DASHBOARD_URL = "${AXIVION_DASHBOARD_URL}"
                        }
                        when {
                            anyOf {
                                expression{
                                    skip_static_code_analysis == false
                                }
                                expression{
                                    BRANCH_TYPE == 'trunk'
                                }
                                expression{
                                    BRANCH_TYPE == 'release'
                                }
                                expression{
                                    BRANCH_TYPE == 'tags'
                                }
                            }
                        }
                        steps {
                            dir('StaticCodeAnalysis') {
                                lock(quantity: 1, resource: 'Axivion') {
                                    loadWindowsScript(name: 'startAxivionAnalysis.bat')
                                    catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                                        bat "startAxivionAnalysis.bat"
                                        recordIssues(healthy: 1, qualityGates: [[threshold: 1, type: 'DELTA', unstable: true]],tools: [axivionSuite(basedir: 'StaticCodeAnalysis', credentialsId: 'hijenkinssrv1', namedFilter: 'p_show_all', projectUrl: "${AXIVION_PROJECT_URL}")])
                                    }
                                }
                            }
                        }
                    }
                    stage('Dynamic Tests') {
                        options {
                            skipDefaultCheckout true
                        }
                        environment {
                            SVN_URL = "${SVN_URL}"
                            VC_Manage_Project = "${VC_Manage_Project}"
                        }
                        when {
                            anyOf {
                                expression{
                                    skip_dynamic_tests == false
                                }
                                expression{
                                    BRANCH_TYPE == 'trunk'
                                }
                                expression{
                                    BRANCH_TYPE == 'release'
                                }
                                expression{
                                    BRANCH_TYPE == 'tags'
                                }
                            }
                        }
                        steps{
                            build job: '../vcast_pipeline', parameters: [string(name: 'VC_Manage_Project', value: "${VC_Manage_Project}"), string(name: 'Repo_URL', value: "${SVN_URL}")]
                        }
                    }
                    stage('HIL') {
                        environment {
                            PYTHONUNBUFFERED = 1
                            PROJECT_NAME = '7090'
                            PCBA_ID = '5000708269-E002'
                            KEIL_PROJECT_PATH = 'Source\\3-2.uvprojx'
                            DUTTYPE = 'TMS-R'
                        }
                        stages{
                            stage('Dynamic resource tests') {
                                agent {
                                    label "HIL && ${PROJECT_NAME as String}"
                                }
                                options {
                                    skipDefaultCheckout true
                                }
                                environment {
                                    STAGE_PATH = 'Tests\\dynamic_resources'
                                    STAGE_FULL_PATH = "${WORKSPACE}\\${STAGE_PATH}"
                                    STAGE_NAME_FILE = STAGE_NAME.toLowerCase().replace(' ','_')
                                }
                                when {
                                    beforeAgent true
                                    anyOf {
                                        expression{
                                            skip_dynamic_resource_tests == false
                                        }
                                    // expression{
                                    //     BRANCH_TYPE == 'trunk'
                                    // }
                                    // expression{
                                    //     BRANCH_TYPE == 'release'
                                    // }
                                    // expression{
                                    //     BRANCH_TYPE == 'tags'
                                    // }
                                    }
                                }
                                steps {
                                    catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                                        dynamicResourceTests7090(timeout: config.dynamicResourceTestTimeout)
                                    }
                                }
                                post{
                                    always{
                                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                                            postTest()
                                        }
                                    }
                                }
                            }
                            stage ('Qualification tests') {
                                agent {
                                    label 'HIL && 7090'
                                }
                                options {
                                    skipDefaultCheckout true
                                }
                                when {
                                    beforeAgent true
                                    anyOf {
                                        expression{
                                            skip_qualification_tests == false
                                        }
                                        expression{
                                            BRANCH_TYPE == 'trunk'
                                        }
                                        expression{
                                            BRANCH_TYPE == 'release'
                                        }
                                        expression{
                                            BRANCH_TYPE == 'tags'
                                        }
                                    }
                                }
                                environment {
                                    STAGE_PATH = 'Tests\\qualification_tests'
                                    STAGE_FULL_PATH = "${WORKSPACE}\\${STAGE_PATH}"
                                    STAGE_NAME_FILE = STAGE_NAME.toLowerCase().replace(' ', '_')
                                }
                                steps {
                                    catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                                        qualificationTests7090()
                                    }
                                }
                                post{
                                    always{
                                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                                            postTest()
                                        }
                                    }
                                }
                            }
                            stage ('System tests') {
                                agent {
                                    label 'HIL && 7090'
                                }
                                options {
                                    skipDefaultCheckout true
                                }
                                when {
                                    beforeAgent true;
                                    anyOf {
                                        expression{
                                            skip_system_tests == false
                                        }
                                        expression{
                                            BRANCH_TYPE == 'trunk'
                                        }
                                        expression{
                                            BRANCH_TYPE == 'release'
                                        }
                                        expression{
                                            BRANCH_TYPE == 'tags'
                                        }
                                    }
                                }
                                environment {
                                    STAGE_PATH = 'Tests\\system_tests'
                                    STAGE_FULL_PATH = "${WORKSPACE}\\${STAGE_PATH}"
                                    STAGE_NAME_FILE = STAGE_NAME.toLowerCase().replace(' ', '_')
                                }
                                steps {
                                    catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                                        systemTests7090()
                                    }
                                }
                                post{
                                    always {
                                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                                            postTest()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        post {
            cleanup {
                cleanWs()
            }
            always {
                script {
                    // copy reports from agent to master if availabale
                    try {
                        unstash 'qualification_tests_report'
                    } catch (e) {
                        print "Unstash 'qualification_tests_report' failed, ignoring"
                    }

                    // copy reports from agent to master if available
                    try {
                        unstash 'system_tests_report'
                    } catch (e) {
                        print "Unstash 'system_tests_report' failed, ignoring"
                    }

                    // copy reports from agent to master if available
                    try {
                        unstash 'dynamic_resource_tests_report'
                    } catch (e) {
                        print "Unstash 'dynamic_resource_tests_report' failed, ignoring"
                    }

                    // if no tests are executed, skip creating zip archive of test reports
                    if (!skip_static_resource_tests || !skip_dynamic_tests || !skip_dynamic_resource_tests || !skip_qualification_tests || !skip_system_tests){

                        // create folder for collecting
                        fileOperations([folderCreateOperation(folderPath: 'TestReports')])

                        // add vectorcast reports to collection
                        fileOperations([fileCopyOperation(excludes: '',
                                            flattenFiles: true,
                                            includes: 'management/*',
                                            targetLocation: 'TestReports')])

                        // add all html reports to collection
                        fileOperations([fileCopyOperation(excludes: '',
                                            flattenFiles: true,
                                            includes: '**/*_report.html',
                                            targetLocation: 'TestReports')])

                        // create zip file for mail report
                        zip zipFile: "TestReports_${env.SW_VER}.zip", archive: false, glob: 'TestReports/**'

                        archiveArtifacts allowEmptyArchive: true, artifacts: "TestReports_*.zip, TestReports/** ", caseSensitive: true, defaultExcludes: true, fingerprint: true, onlyIfSuccessful: false
                    }
                }
            }
        }
    }
}
