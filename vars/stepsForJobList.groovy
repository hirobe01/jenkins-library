// ===============================================================
//
// Function : stepsForJobList
// Inputs   : localEnvList - a list of [compiler, testsuite, environments] that need to be executed
// Action   : Loops of list and calls " transformIntoStep
// Returns  : A job that can be executed
// Notes    : Use to get a list of system and unit tests jobs
//
// ===============================================================
// def stepsForJobList(localEnvList) {
def call(localEnvList, VC_useOneCheckoutDir, VC_usingSCM, VC_EnvSetup, VC_Build_Preamble, VC_waitTime, VC_waitLoops, VC_Manage_Project, VC_UseCILicense, VC_useCBT, VC_EnvTeardown, VC_sharedArtifactDirectory, VC_UnstablePhrases, VC_FailurePhrases) {
    jobList = [:]
    localEnvList.each {
        jobList[it] =  transformIntoStep(it, VC_useOneCheckoutDir, VC_usingSCM, VC_EnvSetup, VC_Build_Preamble, VC_waitTime, VC_waitLoops, VC_Manage_Project, VC_UseCILicense, VC_useCBT, VC_EnvTeardown, VC_sharedArtifactDirectory, VC_UnstablePhrases, VC_FailurePhrases)
    }

    return jobList
}