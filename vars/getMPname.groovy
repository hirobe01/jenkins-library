// ===============================================================
//
// Function : getMPname
// Inputs   : None
// Action   : Returns the base name
// Notes    : Used for creating report name
//
// ===============================================================

// def getMPname() {
def call(VC_Manage_Project) {
    // get the manage projects full name and base name
    def mpFullName = VC_Manage_Project.split("/")[-1]
    def mpName = ""
    if (mpFullName.toLowerCase().endsWith(".vcm")) {
        mpName = mpFullName.take(mpFullName.lastIndexOf('.'))
    } else {
        mpName = mpFullName
    }
    return mpName
}