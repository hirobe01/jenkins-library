def call(Map config = [:]) {
    cleanWs()

    unstash 'dynamic_resource_tests'

    dir('Tests/dynamic_resources') {
        loadWindowsScript(name: 'checkHitestRunning.bat')
        bat 'checkHitestRunning.bat'

        loadWindowsScript(name: 'checkUvisionRunning.bat')
        bat 'checkUvisionRunning.bat'

        loadWindowsScript(name: 'setupPython.bat')
        bat 'setupPython.bat'

        // compare test bench config to requirements/environment
        loadPythonScript(name: 'conf2prop.py')
        bat '.venv\\Scripts\\python.exe conf2prop.py -c C:/testbench/config.json -o properties.txt -e PCBA_ID -e PROJECT_NAME'
        // set properties file as env
        script {
            def props = readProperties file: 'properties.txt'
            env.PCBA_SN = props.PCBA_SN
            env.POWER_SUPPLY_NAME = props.POWER_SUPPLY_NAME
            env.POWER_SUPPLY_SN = props.POWER_SUPPLY_SN
            env.POWER_SUPPLY_PORT = props.POWER_SUPPLY_PORT
            env.POWER_SUPPLY_CHANNEL = props.POWER_SUPPLY_CHANNEL
            env.KEIL_PATH = props.KEIL_PATH
            env.KEIL_VERSION = props.KEIL_VERSION
        }

        bat '%KEIL_PATH%\\UV4\\UV4.exe -r %WORKSPACE%\\%KEIL_PROJECT_PATH% -t Test -j0 -o %STAGE_FULL_PATH%\\ConsoleLog.txt'

        bat 'type ConsoleLog.txt'

        loadPythonScript(name: 'powerSupply.py')
        bat '.venv\\Scripts\\python.exe powerSupply.py --on'

        loadPythonScript(name: 'execKeilDebugTests.py')
        timeout(time: 5, unit: 'MINUTES') {
            bat '.venv\\Scripts\\python.exe execKeilDebugTests.py -t . -s %WORKSPACE%\\Source -c execKeilDebugTests.cfg'
        }
        bat '.venv\\Scripts\\python.exe powerSupply.py --off'

        loadPythonScript(name: 'keilDebugLog2JUnit.py')
        bat '.venv\\Scripts\\python.exe keilDebugLog2JUnit.py -t . -o Results.xml -c keilDebugLog2JUnit.cfg'

        bat ".venv\\Scripts\\python.exe -m junit2htmlreport Results.xml ${STAGE_NAME_FILE}_report.html"
    }
}
