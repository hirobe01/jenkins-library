def call(Map config = [:]) {
    stash includes: "${STAGE_PATH}\\${STAGE_NAME_FILE}_report.html", name: "${STAGE_NAME_FILE}_report", useDefaultExcludes: false

    archiveArtifacts allowEmptyArchive: false, artifacts: "${STAGE_PATH}/**/*.tdms, ${STAGE_PATH}/*.csv, ${STAGE_PATH}/*.log, ${STAGE_PATH}/*.xml, ${STAGE_PATH}/*.html, ${STAGE_PATH}/*.ini, ${STAGE_PATH}/*.txt", caseSensitive: true, defaultExcludes: true, fingerprint: true, onlyIfSuccessful: false

    withChecks(name: "${STAGE_NAME}") {
        junit "${STAGE_PATH}/Results.xml"
    }

    recordIssues(tools: [junitParser(id: "${STAGE_NAME_FILE}", name: "${STAGE_NAME}", pattern: "${STAGE_PATH}/Results.xml")])
}
