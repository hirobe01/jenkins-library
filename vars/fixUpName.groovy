// ===============================================================
//
// Function : fixUpName
// Inputs   : command list
// Action   : Fixup name so it doesn't include / or %## or any other special characters
// Returns  : Fixed up name
// Notes    : Used widely
//
// ===============================================================
//
// def fixUpName(name) {
def call(name) {
    return name.replace("/","_").replaceAll('\\%..','_').replaceAll('\\W','_')
}