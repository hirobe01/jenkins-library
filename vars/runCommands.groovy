// ===============================================================
//
// Function : runCommands
// Inputs   : command list
// Action   : 1. Adds VC Setup calls to beginning of script
//            2. If using CI licenses, it set the appropriate envionment variables
//            3. Replaces keywords for windows/linux
//            4. Calls the command
//            5. Reads the log and return the log file (prints as well)
// Returns  : stdout/stderr from the commands
// Notes    : Used widely
//
// ===============================================================
// def runCommands(cmds) {
def call(cmds,VC_EnvSetup,VC_UseCILicense) {
    def boolean failure = false
    def boolean unstable_flag = false
    def foundKeywords = ""
    def localCmds = """"""

    // clear that old command log
    writeFile file: "command.log", text: ""

    // if its Linux run the sh command and save the stdout for analysis
    if (isUnix()) {
        // add VC setup to beginning of script
        // add extra env vars to make debugging of commands useful
        // add extra env for reports
        localCmds = """
            ${VC_EnvSetup}
            export VCAST_RPTS_PRETTY_PRINT_HTML=FALSE
            export VCAST_NO_FILE_TRUNCATION=1
            export VCAST_RPTS_SELF_CONTAINED=FALSE

            """

        // if using CI licenses add in both CI license env vars
        if (VC_UseCILicense.length() != 0) {
            localCmds += """
                export VCAST_USING_HEADLESS_MODE=1
                export VCAST_USE_CI_LICENSES=1
            """
        }
        cmds = localCmds + cmds
        cmds = cmds.replaceAll("_VECTORCAST_DIR","\\\$VECTORCAST_DIR").replaceAll("_RM","rm -rf ")
        println "Running commands: " + cmds

        // run command in shell
        sh label: 'Running VectorCAST Commands', returnStdout: false, script: cmds

    } else {
        // add VC setup to beginning of script
        // add extra env vars to make debugging of commands useful
        // add extra env for reports
        localCmds = """
            @echo off
            ${VC_EnvSetup}
            set VCAST_RPTS_PRETTY_PRINT_HTML=FALSE
            set VCAST_NO_FILE_TRUNCATION=1
            set VCAST_RPTS_SELF_CONTAINED=FALSE

            """

        // if using CI licenses add in both CI license env vars
        if (VC_UseCILicense.length() != 0) {
            localCmds += """
                set VCAST_USING_HEADLESS_MODE=1
                set VCAST_USE_CI_LICENSES=1
            """
        }
        cmds = localCmds + cmds
        cmds = cmds.replaceAll("_VECTORCAST_DIR","%VECTORCAST_DIR%").replaceAll("_RM","DEL /Q ")
        println "Running commands: " + cmds

        // run command in bat
        bat label: 'Running VectorCAST Commands', returnStdout: false, script: cmds
    }

    // read back the command.log - this is specific to
    log = readFile "command.log"

    println "Commands Output: " + log

    return log
}
