def call(Map config = [:]) {
  def scriptcontents = libraryResource "com/hilite/scripts/python/${config.name}"
  writeFile file: "${config.name}", text: scriptcontents
}
