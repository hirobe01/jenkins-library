def call(Map config = [:]) {
  def scriptcontents = libraryResource "com/hilite/scripts/windows/${config.name}"
  writeFile file: "${config.name}", text: scriptcontents
}
