// ===============================================================

// Function : checkLogsForErrors
// Inputs   : log
// Action   : Scans the input log file to for keywords listed above
// Returns  : found foundKeywords, failure and/or unstable_flag
// Notes    : Used to Check for VectorCAST build errors/problems

// ===============================================================

// def checkLogsForErrors(log) {
def call(log, VC_UnstablePhrases, VC_FailurePhrases) {

    def boolean failure = false;
    def boolean unstable_flag = false;
    def foundKeywords = ""

    // Check for unstable first
    // Loop over all the unstable keywords above
    VC_UnstablePhrases.each {
        if (log.contains(it)) {
            // found a phrase considered unstable, mark the build accordingly
            foundKeywords =  foundKeywords + it + ", "
            unstable_flag = true
        }
    }

    // The check for failure keywords first
    // Loop over all the failure keywords above
    VC_FailurePhrases.each {
        if (log.contains(it)) {
            // found a phrase considered failure, mark the build accordingly
            foundKeywords =  foundKeywords + it + ", "
            failure = true
        }
    }
    if (foundKeywords.endsWith(", ")) {
        foundKeywords = foundKeywords[0..-3]
    }

    return [foundKeywords, failure, unstable_flag]
}