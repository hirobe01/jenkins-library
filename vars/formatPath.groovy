// ===============================================================
//
// Function : formatPath
// Inputs   : directory path
// Action   : on Windows it will change / path seperators to \ (\\)
// Returns  : fixed path
// Notes    : Used to Check for VectorCAST build errors/problems
//
// ===============================================================

// def formatPath(inPath) {
def call(inPath) {
    def outPath = inPath
    if (!isUnix()) {
        outPath = inPath.replace("/","\\")
    }
    return outPath
}