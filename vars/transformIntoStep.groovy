// ===============================================================
//
// Function : transformIntoStep
// Inputs   : inputString containing a single [compiler, test_suite, environment]
// Action   : Uses the input to create a build step in the pipeline job
// Returns  : Build step for a job
// Notes    : This will be executed later in parallel with other jobs
//
// ===============================================================

// def transformIntoStep(inputString) {
def call(inputString, VC_useOneCheckoutDir, VC_usingSCM, VC_EnvSetup, VC_Build_Preamble, VC_waitTime, VC_waitLoops, VC_Manage_Project, VC_UseCILicense, VC_useCBT, VC_EnvTeardown, VC_sharedArtifactDirectory, VC_UnstablePhrases, VC_FailurePhrases) {

    // grab the compiler test_suite and environment out the single line
    def (compiler, test_suite, environment) = inputString.split()

    // set the stashed file name for later
    String stashName = fixUpName("${env.JOB_NAME}_${compiler}_${test_suite}_${environment}-build-execute-stage")

    // return the auto-generated node and job
    // node is based on compiler label
    // this will route the job to a specific node matching that label
    return {
        catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE') {

            // Try to use VCAST_FORCE_NODE_EXEC_NAME parameter.
            // If 0 length or not present, use the compiler name as a node label
            def nodeID = "default"
            try {
                if ("${VCAST_FORCE_NODE_EXEC_NAME}".length() > 0) {
                    nodeID = "${VCAST_FORCE_NODE_EXEC_NAME}"
                }
                else {
                    nodeID = compiler
                }
            } catch (exe) {
               nodeID = compiler
            }

            print "Using NodeID = " + nodeID


            // node definition and job starting here
            node ( nodeID as String ){

                println "Starting Build-Execute Stage for ${compiler}/${test_suite}/${environment}"

                // cleanWs()

                // if we are not using a single checkout directory
                if (!VC_useOneCheckoutDir) {

                    // call the scmStep for each job
                    scmStep()
                }

                // Run the setup step to copy over the scripts
                step([$class: 'VectorCASTSetup'])

                // if we are not using a single checkout directory and using SCM step
                if (VC_usingSCM && !VC_useOneCheckoutDir) {

                    // set options for each manage project pulled out out of SCM
                    setupManageProject()
                }

                // setup the commands for building, executing, and transferring information
                cmds =  """
                    ${VC_EnvSetup}
                    ${VC_Build_Preamble} _VECTORCAST_DIR/vpython "${env.WORKSPACE}"/vc_scripts/managewait.py --wait_time ${VC_waitTime} --wait_loops ${VC_waitLoops} --command_line "--project "${VC_Manage_Project}" ${VC_UseCILicense} --level ${compiler}/${test_suite} -e ${environment} --build-execute ${VC_useCBT} --output ${compiler}_${test_suite}_${environment}_rebuild.html"
                    ${VC_EnvTeardown}
                """


                // setup build lot test variable to hold all VC commands results for this job
                def buildLogText = ""

                // run the build-execute step and save the results
                buildLogText = runCommands(cmds, VC_EnvSetup, VC_UseCILicense)

                def foundKeywords = ""
                def boolean failure = false
                def boolean unstable_flag = false

                // check log for errors/unstable keywords
                (foundKeywords, failure, unstable_flag) = checkLogsForErrors(buildLogText, VC_UnstablePhrases, VC_FailurePhrases)

                // if we didn't fail and don't have a shared artifact directory - we may have to copy back build directory artifacts...
                if (!failure && VC_sharedArtifactDirectory.length() == 0) {

                    // if we are using an SCM checkout and we aren't using a single checkout directory, we need to copy back build artifacts
                    if (VC_usingSCM && !VC_useOneCheckoutDir) {
                        def fixedJobName = fixUpName("${env.JOB_NAME}")
                        buildLogText += runCommands("""_VECTORCAST_DIR/vpython "${env.WORKSPACE}"/vc_scripts/copy_build_dir.py ${VC_Manage_Project} ${compiler}/${test_suite} ${fixedJobName}_${compiler}_${test_suite}_${environment} ${environment}""", VC_EnvSetup, VC_UseCILicense)
                    }
                }

                // write a build log file for compiler/test suite/environment
                writeFile file: "${compiler}_${test_suite}_${environment}_build.log", text: buildLogText

                // no cleanup - possible CBT
                // use individual names
                def fixedJobName = fixUpName("${env.JOB_NAME}")

                // save artifact in a "stash" to be "unstashed" by the main job
                stash includes: "${compiler}_${test_suite}_${environment}_build.log, **/${compiler}_${test_suite}_${environment}_rebuild.html, execution/*.html, management/*${compiler}_${test_suite}_${environment}*, xml_data/*${compiler}_${test_suite}_${environment}*, ${fixedJobName}_${compiler}_${test_suite}_${environment}_build.tar", name: stashName as String

                println "Finished Build-Execute Stage for ${compiler}/${test_suite}/${environment}"

                // check log for errors/unstable keywords again since the copy build dir could have thrown errors/unstable keywords
                (foundKeywords, failure, unstable_flag) = checkLogsForErrors(buildLogText, VC_UnstablePhrases, VC_FailurePhrases)

                // if something failed, raise an error
                if (failure) {
                    error ("Error in Commands: " + foundKeywords)

                // else if something made the job unstable, mark as unsable
                } else if (unstable_flag) {
                    unstable("Triggering stage unstable because keywords found: " + foundKeywords)
                }
            }
        }
    }
}
