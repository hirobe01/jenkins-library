def call(Map config = [:]) {
    dir('Tests/static_resources') {
        loadWindowsScript(name: 'setupPython.bat')
        bat 'setupPython.bat'

        loadPythonScript(name: 'linkerMapParser.py')
        bat '.venv\\Scripts\\python.exe linkerMapParser.py -i ..\\..\\Source\\Listings\\3-2.map -o . -c linkerMapParser.cfg'

        plot csvFileName: 'plot-RWSize.csv', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: 'RWSize.csv', inclusionFlag: 'OFF', url: '']], description: 'RW Size (RW Data + ZI Data) of Objects without Libraries', group: 'Image component sizes', style: 'stackedArea', title: 'RW Size', yaxis: 'Byte'
        plot csvFileName: 'plot-ROSize.csv', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: 'ROSize.csv', inclusionFlag: 'OFF', url: '']], description: 'RO Size (Code + RO Data) of Objects without Libraries', group: 'Image component sizes', style: 'stackedArea', title: 'RO Size', yaxis: 'Byte'
        plot csvFileName: 'plot-TotalROSize.csv', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: 'TotalROSize.csv', inclusionFlag: 'OFF', url: '']], description: 'Total RO Size (Code + RO Data)', group: 'Totals', style: 'Line', title: 'Total RO Size', yaxis: 'Byte', yaxisMinimum: '0.0'
        plot csvFileName: 'plot-TotalRWSize.csv', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: 'TotalRWSize.csv', inclusionFlag: 'OFF', url: '']], description: 'Total RW Size (RW Data + ZI Data)', group: 'Totals', style: 'Line', title: 'Total RW Size', yaxis: 'Byte', yaxisMinimum: '0.0'
        plot csvFileName: 'plot-TotalROMSize.csv', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: 'TotalROMSize.csv', inclusionFlag: 'OFF', url: '']], description: 'Total ROM Size (Code + RW Data + RO Data)', group: 'Totals', style: 'Line', title: 'Total ROM Size', yaxis: 'Byte', yaxisMinimum: '0.0'

        bat ".venv\\Scripts\\python.exe -m junit2htmlreport Results.xml ${STAGE_NAME_FILE}_report.html"
    }
}
