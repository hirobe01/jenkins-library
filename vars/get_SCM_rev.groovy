// ===============================================================
//
// Function : get_SCM_rev
// Inputs   : None
// Action   : Returns SCM revision from git or svn
// Notes    : Used for TESTinsight Command
//
// ===============================================================

// def get_SCM_rev() {
def call(VC_TESTinsights_SCM_Tech) {
    def scm_rev = ""
    def cmd = ""

    if (VC_TESTinsights_SCM_Tech=='git') {
        cmd = "git rev-parse HEAD"
    } else {
        cmd = "svn info --show-item revision"
    }

    if (isUnix()) {
        scm_rev = sh returnStdout: true, script: cmd
    } else {
        cmd = "@echo off \n " + cmd
        scm_rev = bat returnStdout: true, script: cmd
    }

    println "Git Rev Reply " + scm_rev.trim() + "***"
    return scm_rev.trim()
}