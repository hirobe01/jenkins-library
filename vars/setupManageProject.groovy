// ===============================================================
//
// Function : setupManageProject
// Inputs   : none
// Action   : Issues commands needed to run manage project from
//            .vcm and environment defintions
// Returns  : None
// Notes    : Used once per manage project checkout
//
// ===============================================================

// def setupManageProject() {
def call(VC_waitTime, VC_waitLoops, VC_Manage_Project, VC_UseCILicense, VC_sharedArtifactDirectory, VC_EnvSetup) {
    def cmds = """
        _RM *_rebuild.html
        _VECTORCAST_DIR/vpython "${env.WORKSPACE}"/vc_scripts/managewait.py --wait_time ${VC_waitTime} --wait_loops ${VC_waitLoops} --command_line "--project "${VC_Manage_Project}" ${VC_UseCILicense} ${VC_sharedArtifactDirectory} --status"
        _VECTORCAST_DIR/vpython "${env.WORKSPACE}"/vc_scripts/managewait.py --wait_time ${VC_waitTime} --wait_loops ${VC_waitLoops} --command_line "--project "${VC_Manage_Project}" ${VC_UseCILicense} --force --release-locks"
        _VECTORCAST_DIR/vpython "${env.WORKSPACE}"/vc_scripts/managewait.py --wait_time ${VC_waitTime} --wait_loops ${VC_waitLoops} --command_line "--project "${VC_Manage_Project}" ${VC_UseCILicense} --config VCAST_CUSTOM_REPORT_FORMAT=HTML"
    """

    runCommands(cmds, VC_EnvSetup, VC_UseCILicense)
}
