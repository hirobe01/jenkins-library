REM check if HiTest instance is running
@echo off
setlocal

tasklist /fi "ImageName eq HiTest.exe" /fo csv 2>NUL | find /I "HiTest.exe">NUL

if "%ERRORLEVEL%"=="0" (
echo HiTest is running
EXIT /B 1
) else (
EXIT /B 0
)
