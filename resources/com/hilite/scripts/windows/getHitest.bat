@echo off
setlocal EnableDelayedExpansion 

set base_dir="\\ntcifs\nt\Entwicklung\ETesting\04_ET-E\09_Software\HiTest"

REM check if parameter is empty
if "%~1"=="" (
    goto :blankparameter
) else (
    goto :verifyparameter
)

:verifyparameter
REM check if folder for version exists
SET a=Version %~1
@REM echo %a%
if exist "%base_dir%\%a%\" (
    goto :copystep
) else (
    goto :error
)

:blankparameter
REM if no version is selected get the latest
FOR /F "delims=" %%i IN ('dir %base_dir% /b /ad-h /t:c /o-d') DO (
    SET a=%%i
    GOTO :copystep
)
echo Error: No subfolder found
goto :finalstep

:copystep
REM copy version to local dir
echo Get: %a%

robocopy "%base_dir%\%a%\HiTest" HiTest /s /e /LOG+:robocopy_HiTest.log

if %ERRORLEVEL% EQU 16 echo ***FATAL ERROR*** & goto end
if %ERRORLEVEL% EQU 15 echo OKCOPY + FAIL + MISMATCHES + XTRA & goto end
if %ERRORLEVEL% EQU 14 echo FAIL + MISMATCHES + XTRA & goto end
if %ERRORLEVEL% EQU 13 echo OKCOPY + FAIL + MISMATCHES & goto end
if %ERRORLEVEL% EQU 12 echo FAIL + MISMATCHES& goto end
if %ERRORLEVEL% EQU 11 echo OKCOPY + FAIL + XTRA & goto end
if %ERRORLEVEL% EQU 10 echo FAIL + XTRA & goto end
if %ERRORLEVEL% EQU 9 echo OKCOPY + FAIL & goto end
if %ERRORLEVEL% EQU 8 echo FAIL & goto end
if %ERRORLEVEL% EQU 7 echo OKCOPY + MISMATCHES + XTRA & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 6 echo MISMATCHES + XTRA & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 5 echo OKCOPY + MISMATCHES & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 4 echo MISMATCHES & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 3 echo OKCOPY + XTRA & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 2 echo XTRA & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 1 echo OKCOPY & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 0 echo No Change & goto end
:end  

goto :finalstep

:error
echo Error: Folder %a% not found!

:finalstep
endlocal