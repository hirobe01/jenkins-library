@echo off
setlocal EnableDelayedExpansion 

set base_dir=\\ntcifs\nt\Entwicklung\ETesting\04_ET-E\09_Software\HiTest Plugins
echo %base_dir%

REM check if 1st parameter is empty
if "%1"=="" (
    goto :erroremptyparameter
) 
set plugin_name=%1
@REM echo plugin name: %plugin_name%
set plugin_dir=%base_dir%\%plugin_name%
@REM echo plugin dir: %plugin_dir%    


REM check if 2nd parameter is empty
if "%2"=="" (
    goto :latestversion
) else (
    goto :verifyversion
)

:verifyversion
REM check if folder for version exists
SET a=Version %2
if exist "%plugin_dir%\%a%\" (
    set plugin_version_dir=!plugin_dir!\!a!\
    @REM echo plugin version dir: !plugin_version_dir!
    goto :copystep
) else (
    goto :errorversion
)

:latestversion
REM if no version is selected get the latest
FOR /F "delims=" %%i IN ('dir "%plugin_dir%" /b /ad-h /t:c /o-d') DO (
    SET a=%%i
    set plugin_version_dir=%plugin_dir%\%%i
    goto :copystep
)
echo Error: No subfolder found
goto :eof

:copystep
REM copy version to local dir
echo Get: %plugin_version_dir%

robocopy "%plugin_version_dir%\%plugin_name%" "HiTest\Plugins\%plugin_name%" /s /e /LOG+:robocopy_%plugin_name%.log

if %ERRORLEVEL% EQU 16 echo ***FATAL ERROR*** & goto end
if %ERRORLEVEL% EQU 15 echo OKCOPY + FAIL + MISMATCHES + XTRA & goto end
if %ERRORLEVEL% EQU 14 echo FAIL + MISMATCHES + XTRA & goto end
if %ERRORLEVEL% EQU 13 echo OKCOPY + FAIL + MISMATCHES & goto end
if %ERRORLEVEL% EQU 12 echo FAIL + MISMATCHES& goto end
if %ERRORLEVEL% EQU 11 echo OKCOPY + FAIL + XTRA & goto end
if %ERRORLEVEL% EQU 10 echo FAIL + XTRA & goto end
if %ERRORLEVEL% EQU 9 echo OKCOPY + FAIL & goto end
if %ERRORLEVEL% EQU 8 echo FAIL & goto end
if %ERRORLEVEL% EQU 7 echo OKCOPY + MISMATCHES + XTRA & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 6 echo MISMATCHES + XTRA & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 5 echo OKCOPY + MISMATCHES & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 4 echo MISMATCHES & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 3 echo OKCOPY + XTRA & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 2 echo XTRA & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 1 echo OKCOPY & EXIT /B 0 & goto end
if %ERRORLEVEL% EQU 0 echo No Change & goto end
:end  
goto :finalstep

:errorversion
echo Error: Version %* does not exist
goto :finalstep

:erroremptyparameter
echo Error: No plugin selected
goto :finalstep

:finalstep
endlocal