REM check if uVision instance is running
@echo off
setlocal

tasklist /fi "ImageName eq UV4.exe" /fo csv 2>NUL | find /I "UV4.exe">NUL

if "%ERRORLEVEL%"=="0" (
echo uVision is running
EXIT /B 1
) else (
EXIT /B 0
)
