import csv
import re
import argparse
from pathlib import Path
import configparser
import json
from junit_xml import TestSuite, TestCase
from datetime import datetime

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def parseArguments():
   parser = argparse.ArgumentParser(description='parse linker map ,export data to csv-files and test resource consumtion')
   parser.add_argument('-c','--config',help='file path to config cfg-file', required=True, type=Path)
   parser.add_argument('-i','--input',help='file path to linker map-file', required=True, type=Path)
   parser.add_argument('-o','--output',help='folder path for csv-files and junit-files', required=True, type=Path)

   return parser.parse_args()

def getConfig(fileName):
   config = configparser.ConfigParser()
   config.read(fileName)

   return config

def parseMapFile(file,config):

    re_section_name = config["DEFAULT"]["re_section_name"]
    re_section_divider = config["DEFAULT"]["re_section_divider"]
    re_sub_section_divider = config["DEFAULT"]["re_sub_section_divider"]

    object_sizes = []
    total_sizes = []

    try:

        with open(file, "r") as mf:
            section_start = False
            sub_section = 0
            section_name = ''
            for line in mf.readlines():
                if line :
                    match_section_divider = re.search(re_section_divider, line)
                    match_section_name = re.search(re_section_name, line)
                    match_sub_section_divider = re.search(re_sub_section_divider, line)

                    if section_name and match_sub_section_divider:
                        sub_section += 1

                    if not section_start and match_section_divider:
                        section_start = True
                        section_name = ''
                        sub_section = 0

                    if section_start and match_section_name :
                        section_name = match_section_name.string
                        section_start = False

                    if section_name.startswith('Image component sizes') and sub_section == 0:
                        c = re.compile(" +(\d+) +(\d+) +(\d+) +(\d+) +(\d+) +(\d+) +(\w+.*)")
                        m = c.match(line)
                        if m: 
                            #print(m.groups())
                            # move object name from last column to first
                            data_list = list(m.groups())
                            first_element = data_list.pop()
                            data_list = list(map(int, data_list))
                            data_list.insert(0,first_element)

                            # calc Total RW  Size (RW Data + ZI Data) and Total RO  Size (Code + RO Data)  and ROM Size (Code + RO Data + RW Data)
                            #print(data_list)
                            RW_size = data_list[4] + data_list[5]
                            RO_size = data_list[1] + data_list[3]
                            ROM_size = data_list[1] + data_list[3] + data_list[5]
                            data_list.extend([RW_size,RO_size,ROM_size])
                            #print(data_list)

                            object_sizes.append(data_list)
                    
                    if not section_name and section_start and sub_section == 0:
                        c = re.compile("[ ]+(Total.+Size).+\) +(\d+)")
                        m = c.match(line)
                        if m: 
                            data_list = list(m.groups())
                            data_list[1] = int(data_list[1])

                            total_sizes.append(data_list)     

    except:
        print(f'{bcolors.FAIL}ERROR: parsing file failed: {file} {bcolors.ENDC}')
    
    else: 
        print(f'{bcolors.OKBLUE}parsing finished{bcolors.ENDC}')  
        return object_sizes,total_sizes

def writeCSV(file,data):
    try:
        with open(file, 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(data)
    except:
        print(f'{bcolors.FAIL}ERROR: write csv-file failed: {file} {bcolors.ENDC}')

def objects2csv(objects,output_folder):
    objects.insert(0, ['Object Name','Code (inc. data)','data','RO Data','RW Data','ZI Data','Debug','RW Size','RO Size','ROM Size' ])
    objects = zip(*objects)
    
    data_list = list(objects)
    header = data_list.pop(0)

    for row in data_list:
        name = row[0]
        filename = name.replace(" ", "")
        filepath = output_folder / f'{filename}.csv'
        csv_data = [header,row]
    
        writeCSV(filepath,csv_data)

def totals2csv(totals,testspec,output_folder):

    header = ['Name', 'Total', 'Limit']

    for row in totals:
        name = row[0]
        for test in testspec['test_cases']:
            if name == test['object']:
                limit = int(test['upper_limit'])
                row.append(limit)
        filename = name.replace(" ", "")
        filepath = output_folder / f'{filename}.csv'
        csv_data = [header,row]

        writeCSV(filepath,csv_data)

def exportCSV(data,testspec,output_folder):

    objects = data[0]
    totals = data[1]

    totals2csv(totals,testspec,output_folder)

    objects2csv(objects,output_folder)

    print(f'{bcolors.OKBLUE}csv-export finished{bcolors.ENDC}')  

def readJsonFile(path):
    with open(path) as json_file:
        data = json.load(json_file)
    return data

def getTestspec(config):

    file = config["DEFAULT"]["testspec"]

    try:
        data = readJsonFile(file)
    except:
        print(f'{bcolors.FAIL}ERROR: loading limit file failed: {file} {bcolors.ENDC}')
    else:
        return data

def runTests(data,testspec,config):

    object_sizes = data[0]
    total_sizes = data[1]
    test_cases = list()

    test_suite_name = config["DEFAULT"]["test_suite_name"]

    total_sizes.pop(0) # remove header

    for total in total_sizes:
        total_name = total[0]
        value = total[1]
        
        for test in testspec['test_cases']:
            if total_name == test['object']:
                upper_limit = int(test['upper_limit'])
                name = test['object']
                test_class_name = test_suite_name+'.'+test['type']

                test_case = TestCase(name=test['id'], classname=test_class_name,stdout=f'name: {name}, value: {value}, upper limit: {upper_limit}', file=name)
                        
                if value > upper_limit:
                    test_case.add_failure_info('upper limit exceeded')

                test_cases.append(test_case)   

    for object in object_sizes:
        name = object[0]
        value = {'RW Size': object[7],'RO Size': object[8],'ROM Size': object[9]}
        for test in testspec['test_cases']:
            if name == test['object']:
                for type in value.keys():
                    if type == test['type']:
                        componentname = test['object'].removesuffix('.o').upper()
                        test_class_name = f"{test_suite_name}.{componentname}"
                        test_case = TestCase(name=test['id'], classname=test_class_name,stdout=f"object name: {name}, value: {value[type]}, upper limit: {test['upper_limit']}", file=test['object'])
                        
                        if value[type] > test['upper_limit']:
                            test_case.add_failure_info('upper limit exceeded')

                        test_cases.append(test_case)   
                                   

    ts = TestSuite(name=test_suite_name, test_cases=test_cases, id=1, timestamp=datetime.now())

    print(f'{bcolors.OKBLUE}tests finished{bcolors.ENDC}')
    return ts   

def WriteJunitFile(filename,path,testsuite):
    try:
        with open(path/filename, 'w') as f:
            TestSuite.to_file(f, [testsuite], prettyprint=True)
    except:
        print(f'{bcolors.FAIL}ERROR: write xml failed{bcolors.ENDC}')             

def main():

    args = parseArguments()

    input_file = args.input
    output_folder = args.output
    config_file = args.config

    # basename = Path(__file__).stem
    # config = getConfig(f'{basename}.cfg')   
    config = getConfig(config_file)   

    data = parseMapFile(input_file,config)

    testspec = getTestspec(config)

    exportCSV(data,testspec,output_folder)

    testsuite = runTests(data,testspec,config)
    
    JunitFile = config["DEFAULT"]["junit_file_name"]
    WriteJunitFile(JunitFile,output_folder,testsuite)

main()