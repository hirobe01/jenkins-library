from distutils.log import debug
import os
import sys
import shutil
import argparse
from pathlib import Path
import configparser
import time
from datetime import datetime

debugMode = False

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def dir_path(path):
    if Path(path).exists():
        return Path(path)
    else:
        raise argparse.ArgumentTypeError(f"{path} does not exist")

def parse_arguments():
   parser = argparse.ArgumentParser(description='Execute debug scripts with Keil µVision debugger')
   parser.add_argument('-t','--testScriptDir',help='directory with tests scripts', required=True, type=dir_path)
   parser.add_argument('-s','--sourceDir',help='directory with source code', required=True, type=dir_path)
   parser.add_argument('-c','--config',help='config file', required=False, type=Path, default='execTests.cfg')
   parser.add_argument('-d','--debug',help='debug mode (without executing keil)', action='store_true')

   return parser.parse_args()

def get_config(fileName):
   config = configparser.ConfigParser()
   config.read(fileName)

   return config

def copy_debug_file(testScriptFile,debugFile):

   try:
      shutil.copyfile(testScriptFile, debugFile)
   except:
      print(f'{bcolors.FAIL}ERROR: copy ini-Script failed{bcolors.ENDC}')
      return True
   else:
      return False

def exec_keil_debug(keilLogFile, keilProjectPath, keilPath, testLogFile):
   global debugMode
   keilProjectFilePath = Path(keilProjectPath)
   keilParameter = f'-d ..\\..\\{keilProjectFilePath} -t "Basic" -j0 -o "{keilLogFile}"'
   keilExe = f"{keilPath}\\UV4\\UV4.exe"
   try:
      if debugMode:
         print(f'{bcolors.WARNING}DEBUG: keil is not executed{bcolors.ENDC}')
         print(f'{bcolors.WARNING}{keilExe} {keilParameter}{bcolors.ENDC}')
      else: 
         os.system(f"{keilExe} {keilParameter}")
   except:
      print(f'{bcolors.FAIL}ERROR: execute keil debug failed{bcolors.ENDC}')
      print_file(keilLogFile)
      return True
   else: 
      print_file(keilLogFile)
      print_file(testLogFile)
      return False      

def print_file(FilePath):
   try:
      with open(FilePath, 'r') as f:
         print(f.read())
   except:
      print(f'{bcolors.FAIL}ERROR: print failed: {FilePath}{bcolors.ENDC}')
      return True
   else: 
      return False

def convert_log(LogFile,ResultFile):
   try:
      #print(f".venv\Scripts\python.exe TestLog2JUnit.py {LogFile} {ResultFile}")
      os.system(f".venv\Scripts\python.exe TestLog2JUnit.py -i {LogFile} -o {ResultFile}")
   except:
      print(f'{bcolors.FAIL}ERROR: convert log 2 xml failed{bcolors.ENDC}')

def inplace_change(filename, old_string, new_string):
    # Safely read the input filename using 'with'
    with open(filename) as f:
        s = f.read()
        if old_string not in s:
            print('"{old_string}" not found in {filename}.'.format(**locals()))
            return

    # Safely write the changed content, if found in the file
    with open(filename, 'w') as f:
        print('Changing "{old_string}" to "{new_string}" in {filename}'.format(**locals()))
        s = s.replace(old_string, new_string)
        f.write(s)

def get_environment_values(dict):
    for key in dict.keys():
        dict[key] = os.getenv(key)

    for key,value in dict.items():
        if(not value ):
            print(f"{bcolors.FAIL}Error: Env value for {key} missing{bcolors.ENDC}")

    return dict

def main():
   global debugMode

   args = parse_arguments()

   testScriptDir = args.testScriptDir
   sourceDir = args.sourceDir
   config_file = args.config
   debugMode = args.debug

   config = get_config(config_file)

   debugFileName = config["DEFAULT"]["DebugIniFileName"]
   debugFile = f"{sourceDir}\{debugFileName}"
   testScriptPattern = config["DEFAULT"]["TestScriptPattern"]

   env = dict.fromkeys(["PROJECT_NAME","PCBA_ID","PCBA_SN","KEIL_VERSION","KEIL_PATH","KEIL_PROJECT_PATH","SW_VER","SVN_REVISION"])
        
   env_values = get_environment_values(env)

   testScriptFileList = list(Path(testScriptDir).glob(testScriptPattern))

   if not testScriptFileList:
      #print(f'\r\n{bcolors.FAIL}ERROR no test scripts found{bcolors.ENDC}\r\n')
      #raise ValueError(f'\r\n{bcolors.FAIL}ERROR no test scripts found{bcolors.ENDC}\r\n')
      sys.exit(f'{bcolors.FAIL}ERROR no test scripts found{bcolors.ENDC}')

   # loops trough all files according to pattern
   print(f'\r\n{bcolors.HEADER}Start executing test scripts{bcolors.ENDC}\r\n')
   for testScriptFile in testScriptFileList:
      start_time = time.time()
    
      print(f'\r\n{bcolors.BOLD}Execute {testScriptFile.name}{bcolors.ENDC}\r\n')

      testLogFile = testScriptFile.parent / (testScriptFile.stem + ('_Test.log'))
      keilLogFile = testScriptFile.parent / (testScriptFile.stem + ('_Console.log'))
      #resultFile = testScriptFile.parent / (testScriptFile.stem + ('_Results.xml'))

      error = copy_debug_file(testScriptFile,debugFile)

      # Append EXIT to script to ensure clean exit of test
      with open(debugFile, 'a') as file1:
         file1.write("\nEXIT\n")

      if not error:
         error = exec_keil_debug(keilLogFile, env_values['KEIL_PROJECT_PATH'], env_values['KEIL_PATH'], testLogFile)

      # if not error:
      #    convert_log(testLogFile,resultFile)

      elapsed_time = time.time() - start_time
      
      additional_info = f"'project_name': '{env_values['PROJECT_NAME']}','pcba_id': '{env_values['PCBA_ID']}','pcba_sn': '{env_values['PCBA_SN']}','keil_version': '{env_values['KEIL_VERSION']}','svn_revision': '{env_values['SVN_REVISION']}','sw_ver': '{env_values['SW_VER']}','start_time': '{datetime.fromtimestamp(start_time)}','time': {elapsed_time},"

      inplace_change(testLogFile, "'id'", f"{additional_info}'id'")

   print(f'\r\n{bcolors.HEADER}Finish executing test scripts{bcolors.ENDC}\r\n')

   # if errorMsg:
   #    sys.exit(errorMsg)

main()