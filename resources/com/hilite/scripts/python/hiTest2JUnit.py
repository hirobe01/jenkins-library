from datetime import datetime
from junit_xml import TestSuite, TestCase
from nptdms import TdmsFile
import json
import re
import glob
import argparse
from pathlib import Path
import configparser
from itertools import groupby
import os

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def parse_arguments():
   parser = argparse.ArgumentParser(description='Convert test log to JUnit xml', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
   parser.add_argument('-i','--input',help='path to search for Result.tdms files', default='**/Calc/Results.tdms')
   parser.add_argument('-o','--output',help='output path for Result.xml file', default='Results.xml', type=Path)
   parser.add_argument('-c','--config',help='path to cfg', required=False, type=Path, default='hiTest2JUnit.cfg')

   return parser.parse_args()

def get_config(fileName):
   config = configparser.ConfigParser()
   config.read(fileName)

   return config

def ReadTdmsFile(file):

    try:
        tdms = TdmsFile.read(file)
    except:
        print(f"{bcolors.FAIL}Error Parsing File{bcolors.ENDC}")
        
    else:
        return tdms
# define a function for key
def key_func(k):
    return k['Group']

def get_environment_values(dict):
    for key in dict.keys():
        dict[key] = os.getenv(key)

    for key,value in dict.items():
        if(not value ):
            print(f"{bcolors.FAIL}Error: Env value for {key} missing{bcolors.ENDC}")

    return dict

def Tdms2TestSuite(tdms,TestCaseClassname,TestSuitePackage,TestSpecIdPattern):

    #group_name = 'Gesamt_Tabelle'
    group_name = 'Results_Table'
    if (group := tdms[group_name]):

        results_df = group.as_dataframe(time_index=False, absolute_time=False, scaled_data=True)
        # print(df.dtypes)
        results_dict_list = results_df.to_dict(orient='records')
        #print(results_dict_list)
        
        env = dict.fromkeys(["PROJECT_NAME","PCBA_ID","PCBA_SN","SW_VER","SVN_REVISION"])
        env_values = get_environment_values(env)

        # group_pattern = '_([0-9]{3,5})_.*'
        # r = re.compile(group_pattern)
        r = re.compile(TestSpecIdPattern)

        for results_dict in results_dict_list:
            m = re.match(r, results_dict.get('Name'))
            results_dict.update({"Group": m.groups()[0]})
            results_dict.update(env_values)
            #print(m.groups()[0])

        # print(results_dict_list)
        
        # sort results_dict_list data by 'Group' key.
        results_dict_list = sorted(results_dict_list, key=key_func)
        
        test_suites = list()
        test_cases = list()
        test_suite_id = 0

        for key, value in groupby(results_dict_list, key_func):
            # print(key)
            test_suite_id += 1
            TestSuiteName = f"{env_values.get('PROJECT_NAME')}-{key}"
            # print(list(value))
            results_grouped = list(value)
            group_evaluation = 1
            failure_info=''

            for result in results_grouped: 

                if (result.get('Passed')==0):
                    group_evaluation = 0
                    failure_info+=f"{result.get('Name')}, "

            test_case = TestCase(name=TestSuiteName, classname=f"{TestSuitePackage}.{TestSuiteName}", stdout=json.dumps(results_grouped,indent=2))
            if(group_evaluation==0):
                test_case.add_failure_info(failure_info)
            test_cases.append(test_case)
            test_suite = TestSuite(name=TestSuiteName, package=TestSuitePackage, test_cases=test_cases, id=test_suite_id, timestamp=datetime.now())
            test_suites.append(test_suite)
            test_cases = list()

        return test_suites
    
    else:
        print(f'{bcolors.FAIL}No group named {group_name} found in tdms-file{bcolors.ENDC}')



def WriteJunitFile(file,testsuites):
     with open(file, 'w') as f:
            TestSuite.to_file(f, testsuites, prettyprint=True)

def main():

    args = parse_arguments()

    input_search_path = args.input
    output_file = args.output
    config_file = args.config

    config = get_config(config_file)
    # basename = Path(__file__).stem
    # config = get_config(f'{basename}.cfg')

    TestSuitePackage = config["DEFAULT"]["TestSuitePackage"]
    TestCaseClassname = config["DEFAULT"]["TestCaseClassname"]
    TestSpecIdPattern = config["DEFAULT"]["TestSpecIdPattern"]

    # TestSuitePackage = 'SystemTests'
    # TestCaseClassname = 'SystemTests'
    # TestSpecIdPattern = '_([0-9]{3,5})_.*'
    
    input_files = glob.glob(input_search_path, recursive=True)
    for input_file in input_files:

        Tdms = ReadTdmsFile(input_file)

        TestSuites = Tdms2TestSuite(Tdms,TestCaseClassname,TestSuitePackage,TestSpecIdPattern)

        WriteJunitFile(output_file,TestSuites)

main()