#!/usr/bin/env python

import json
import os
import argparse
from pathlib import Path

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def parse_arguments():
   parser = argparse.ArgumentParser(description='convert test bench config 2 properties')
   parser.add_argument('-c','--config',help='input path test bench config', required=True, type=Path, default='C:/testbench/config.json')
   parser.add_argument('-o','--output',help='output path for properties file', required=True, type=Path, default='properties.txt')
   parser.add_argument('-e','--env',help='environment variables', required=True, action='append')
   
   return parser.parse_args()

def read_file(file_path):
    try:
        file_content = json.load(open(file_path))
        #print(file_content)
    except:
        print(f"{bcolors.FAIL}Error Parsing File {file_path}{bcolors.ENDC}")
        exit(1)
        
    else:
        return file_content
    
def get_environment_values(dict):
    for key in dict.keys():
        dict[key] = os.getenv(key)

    for key,value in dict.items():
        if(not value ):
            print(f"{bcolors.FAIL}Error: Env value for {key} missing{bcolors.ENDC}")

    return dict

def main():
    
    args = parse_arguments()

    config_file = args.config
    #print(config_file)
    output_file = args.output
    #print(output_file)
    env_list = args.env
    #print(env_list)
    
    config = read_file(config_file)
 
    env = dict.fromkeys(env_list)
        
    env_values = get_environment_values(env)
    
    match_found = False
    for i in config['SETUP']:   
        # check if envs are a subset of the configuration
        if env_values.items() <= i.items():
            #print(i)
            for k, v in i.items():
                with open(output_file, 'a') as f:
                    line = k + '=' + v.replace('\\','\\\\') + '\n'
                    # print(line)
                    f.write(line)
                    match_found = True

    if(not match_found):
        print(f"{bcolors.FAIL}Error: None of the test bench configurations meets the requirements{bcolors.ENDC}")
        print(env_values)
        exit(1)

main()