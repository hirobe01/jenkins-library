import sys
from datetime import datetime
from junit_xml import TestSuite, TestCase
import json
import re
import argparse
from pathlib import Path
import configparser

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def dir_path(path):
    if Path(path).exists():
        return Path(path)
    else:
        raise argparse.ArgumentTypeError(f"{path} does not exist")

def parse_arguments():
   parser = argparse.ArgumentParser(description='Convert test logs to JUnit xml',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#    parser.add_argument('-i','--input',help='path to test log file', required=True, type=Path)
   parser.add_argument('-t','--testlogDir',help='directory with tests logs', required=False,  default='.', type=dir_path)
   parser.add_argument('-o','--output',help='path to result xml', required=False, default='Results.xml', type=Path)
   parser.add_argument('-c','--config',help='path to cfg', required=False, type=Path, default='keilDebugLog2JUnit.cfg')

   return parser.parse_args()

def get_config(fileName):
   config = configparser.ConfigParser()
   config.read(fileName)

   return config

def ReadLogFile(file):

    try:
        log = json.load(file)
    except:
        print(f'{bcolors.FAIL}ERROR: parsing file{bcolors.ENDC}')
        
    else:
        return log

def Json2TestSuite(log,file,TestCaseClassname,TestSuiteName,TestSuitePackage):

    test_cases = list()
    
    if (log):

        for tc in log['TestCases']:
            #print(tc['name'])
            test_case = TestCase(name=tc.get('name'), classname=TestCaseClassname, stdout=json.dumps(tc,indent=2), file=file)
            if (tc.get('evaluation')==0):
                test_case.add_failure_info('test failed')
            elif (tc.get('evaluation')==1):
                pass
            else:
                test_case.add_skipped_info('test skipped')

            test_cases.append(test_case)
 
    else:
        print(f'{bcolors.FAIL}ERRRO: no test results found in log{bcolors.ENDC}')

        test_case = TestCase(name=Path(file).stem, classname=TestCaseClassname, stdout=Path(file).read_text() ,file=file)
        test_case.add_failure_info('test failed')
        test_cases.append(test_case)

    ts = TestSuite(name=TestSuiteName, package=TestSuitePackage, test_cases=test_cases, id=1, timestamp=datetime.now())
        #print(TestSuite.to_xml_string([ts]))

    return ts

def Json2TestCase(log,file,TestCaseClassname,TestSuiteName,TestSuitePackage):
    test_cases = list()
    
    if (log):
        tc_cnt = len(log['TestCases'])
        for tc in log['TestCases']:
            #print(tc['name'])
            TestSuiteName = f"{tc.get('project_name')}-{tc.get('id')}"
            test_case = TestCase(name=TestSuiteName, classname=f"{TestSuitePackage}.{TestSuiteName}", stdout=json.dumps(tc,indent=2), file=file, timestamp=tc.get('start_time'),elapsed_sec=(tc.get('time'))/tc_cnt)
            if (tc.get('evaluation')==0):
                test_case.add_failure_info('test failed')
            elif (tc.get('evaluation')==1):
                pass
            else:
                test_case.add_skipped_info('test skipped')

            test_cases.append(test_case)
            #print(test_case)
 
    else:
        print(f'{bcolors.FAIL}ERRRO: no test results found in log{bcolors.ENDC}')

        test_case = TestCase(name=Path(file).stem, classname=f"{TestSuitePackage}.{TestSuiteName}", stdout=Path(file).read_text() ,file=file)
        test_case.add_failure_info('test failed')
        test_cases.append(test_case)

    return test_cases
          
def WriteJunitFile(file,testsuites):
    try:
        #print(testsuites)
        with open(file, 'w') as f:
            TestSuite.to_file(f, testsuites, prettyprint=True)
    except Exception as error:
        print(f'{bcolors.FAIL}ERROR: write xml failed {error}{bcolors.ENDC}')

def main():

    args = parse_arguments()

    testLogDir = args.testlogDir
    output_file = args.output
    config_file = args.config

    config = get_config(config_file)
    # basename = Path(__file__).stem
    # config = get_config(f'{basename}.cfg')

    TestCaseClassname = config["DEFAULT"]["TestCaseClassname"]
    TestSuiteName = config["DEFAULT"]["TestSuiteName"]
    testLogPattern = config["DEFAULT"]["TestLogPattern"]
    TestSuitePackage = TestSuiteName

    testLogFileList = list(Path(testLogDir).glob(testLogPattern))

    if not testLogFileList:
        sys.exit(f'{bcolors.FAIL}ERROR no test logs found with pattern: {testLogPattern} {bcolors.ENDC}')

    test_cases = list()
    test_suites = list()

    # loops trough all files according to pattern
    print(f'\r\n{bcolors.HEADER}Start converting test logs{bcolors.ENDC}\r\n')
    for testLogFile in testLogFileList:

        print(f'\r\n{bcolors.BOLD}Converting {testLogFile.name}{bcolors.ENDC}\r\n')

        logFile = open(testLogFile)
        TestLog = logFile.read()
        #print(TestLog)

        if not TestLog:
            print(f'{bcolors.FAIL}ERROR: log file is empty{bcolors.ENDC}')
        
        else:

            # remove lines with __^ and lines before
            regex = r"(?:.*\n.*^_+\^[\r\n])+"
            subst = ""
            TestLog = re.sub(regex, subst, TestLog, 0, re.MULTILINE)
            #print(TestLog)

            # remove lines starting with ***
            regex = r"(?:^\*{3}.*[\r\n])+"
            subst = ""
            TestLog = re.sub(regex, subst, TestLog, 0, re.MULTILINE)
            #print(TestLog)

            # remove lines starting with debug:
            regex = r"(?:^debug:.*[\r\n])+"
            subst = ""
            TestLog = re.sub(regex, subst, TestLog, 0, re.MULTILINE)
            #print(TestLog)

            # remove lines starting with * JLink Info:
            regex = r"(?:\* JLink Info:.*[\r\n])+"
            subst = ""
            TestLog = re.sub(regex, subst, TestLog, 0, re.MULTILINE)
            #print(TestLog)
            
            # replace Test Start and End with json 
            regex = r"^printf\(\"<TestStart>\"\);[\r\n]^<TestStart>[\r\n]+((?:^ExecTest.+[\r\n]'.+[\r\n])+)printf\(\"<TestEnd>\"\);[\r\n]<TestEnd>[\r\n]?LOG OFF"
            #print(regex)
            subst = "{\"TestCases\":[\\r\\n\\g<1>]}"
            TestLog = re.sub(regex, subst, TestLog, 0, re.MULTILINE)
            #print(TestLog)

            # # get backup name for case testlog is empty
            # regex = r"(^WEG[0-9]+_.+)[\r\n]"
            # backup_name = re.match(regex,TestLog)
            # #print(backup_name)

            # replace function call with json name
            regex = r"^ExecTest(\(.+\));[\r\n]('.+)[\r\n]"
            subst = "{'parameter':\'\\g<1>\',\\g<2>},\\r\\n"
            TestLog = re.sub(regex, subst, TestLog, 0, re.MULTILINE)
            #print(TestLog)

            try:
                # replace brackets and CR and convert to json
                jsonLog = json.loads(TestLog.replace('},\r\n]}',"}\r\n]}").replace("'","\""))
                #print(json.dumps(jsonLog))
            except:
                jsonLog = {}

            test_cases += Json2TestCase(jsonLog,testLogFile,TestCaseClassname,TestSuiteName,TestSuitePackage)
            testsuite_id = jsonLog['TestCases'][0].get('id')
            TestSuiteName = f"{jsonLog['TestCases'][0].get('project_name')}-{jsonLog['TestCases'][0].get('id')}"
            test_suite = TestSuite(name=TestSuiteName, package=TestSuitePackage, test_cases=test_cases, timestamp=datetime.now(), id = testsuite_id)
            test_suites.append(test_suite)
    #print(test_cases[0])
    
    # ts = TestSuite(name=TestSuiteName, package=TestSuitePackage, test_cases=test_cases, timestamp=datetime.now(), id = testsuite_id)
    #print(TestSuite.to_xml_string([ts]))

    WriteJunitFile(output_file,test_suites)


main()