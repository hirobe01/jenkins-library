#!/usr/bin/env python3

from optparse import Option
import os
from configparser import RawConfigParser as ConfParser
from configparser import Error
import re
import argparse
from pathlib import Path
import glob

def parseArguments():
   parser = argparse.ArgumentParser(description='configure HiTest')
   parser.add_argument('-c','--config',help='Path of settings file Memory.ini', required=False, type=Path, default="HiTest/data/Memory.ini")
   parser.add_argument('-w','--workspace',help='Path of workspace folder', required=False, type=Path, default=Path.cwd())
      
   return parser.parse_args()

def setConfig(config,workspace):
    HiTestConfigPath = '\"' + workspace + '/HiTestConfig\"'
    HiTestSavePath = '\"' + workspace + '/HiTestResults\"'
    section = "Paths"
    options = config.options(section)
    regex = r"^Save"
    try:
        config.set(section,'ConfigRoot',HiTestConfigPath )
        
        for o in options:
            if re.match(regex,o):
                config.set(section,o,HiTestSavePath )

        config.set('ActivePlugins','Hameg','\"Hameg.xml\"' )
        config.set('ActivePlugins','LIN','\"LIN.xml\"' )

    except Error as msg:
        print("Error: Setting options")
        print(msg)

    return config

def saveConfig(config,settings_file):
    try:
        config.write( open(settings_file, 'w') )
    except Error as msg:
        print("Error Saving File")
        print(msg)

def readConfig(settings_file,file_encoding):

    config = ConfParser(allow_no_value=True)
    config.optionxform = str

    try:
        results = config.read(settings_file, encoding=file_encoding)
    except Error as msg:
        print("Error Parsing File")
        print(msg)
    else:
        if results == []:
            print(f"Error: Could not load {settings_file}")
            if not Path.exists(settings_file):
                print(f"Error: Path does not exist {settings_file}")

            else:
                print("Error: An unknown error occurred.")

        else:
            return config
        
def main():
    args = parseArguments()

    config_file = args.config
    workspace_folder = args.workspace    
    workspace = str(workspace_folder).replace('C:\\','/C/').replace('\\','/')

    PCBA_ID = os.getenv('PCBA_ID')
    PROJECT_NAME = os.getenv('PROJECT_NAME')
    PCBA_SN = os.getenv('PCBA_SN')
    SW_VER = os.getenv('SW_VER')
    SVN_REVISION = os.getenv('SVN_REVISION')
    SETUP_PLACE = os.getenv('SETUP_PLACE')
    DUTType = os.getenv('DUTTYPE')


    config_file = Path('HiTest/data/Memory.ini')
    config = readConfig(config_file,'utf-8')
    # print({section: dict(config[section]) for section in config})
    new_config = setConfig(config,workspace)
    # print({section: dict(config[section]) for section in new_config})
    saveConfig(new_config,config_file)

    config_file = Path('HiTestConfig\Macros\Basic.txt')
    config = readConfig(config_file,'utf-8-sig')
    # print({section: dict(config[section]) for section in config})
    # config.add_section('OnHiTestStart')
    config.set('OnHiTestStart','HIL',1)
    # config.add_section(f'{PROJECT_NAME}Config')
    config.set(f'{PROJECT_NAME}Config','DUTName',f'_{PCBA_SN}')
    config.set(f'{PROJECT_NAME}Config','SetupPlace',f'{SETUP_PLACE}')
    saveConfig(config,config_file)

    config_files = glob.glob('HiTestConfig\Configurations\*\Misc\GroupConfig.ini', recursive=True)
    for config_file in config_files:

        # config_file = Path(f'HiTestConfig\Configurations\{DUTType}\Misc\GroupConfig.ini')
        config = readConfig(config_file,'utf-8')
        config.set('Config','Project',PROJECT_NAME)
        config.set('Config','DUTDrawing',PCBA_ID)
        # config.set('Config','TestVariant',PCBA_SN)
        config.set('Config','TestInfo1',f'SW-VER:{SW_VER}')
        config.set('Config','TestInfo2',f'SVN-REV:{SVN_REVISION}')
        saveConfig(config,config_file)

main()           