from RsInstrument import *
import argparse
import configparser
from pathlib import Path
import os

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def parse_arguments():
   parser = argparse.ArgumentParser(description='Control Power Supply ')
   parser.add_argument('--on', action=argparse.BooleanOptionalAction)
   parser.add_argument('--off',action=argparse.BooleanOptionalAction)
   parser.add_argument('--reset',action=argparse.BooleanOptionalAction,help='reset instrument (before on/off)')
   parser.add_argument('-v','--voltage',help='output voltage', required=False, type=float, default=13.5)
   parser.add_argument('-c','--current',help='max output current', required=False, type=float, default=1.0)
   parser.add_argument('-o','--channel',help='output channel', required=False, type=int, default=None)
   parser.add_argument('-p','--port',help='instrument port', required=False, default=None)
      
   return parser.parse_args()

def get_config(fileName):
   config = configparser.ConfigParser()
   config.read(fileName)

   return config

def connect_to_instrument(port, name, sn, verify):
    try:
        instr = RsInstrument(port, True)
    except:
        print(f'{bcolors.FAIL}Error connection to port: {port} failed {bcolors.ENDC}')
    else:
        print(f'{bcolors.OKBLUE}OK connection to port: {port} successfull{bcolors.ENDC}')
        idn = instr.query_str('*IDN?').split(",")
        
        if(verify):

            if (idn[1] == name and idn[2] == sn):
                print(f'{bcolors.OKBLUE}OK verified instrument name: {name} and sn: {sn} {bcolors.ENDC}')
                return instr
            else:
                print(f'{bcolors.FAIL}Error verifying instrument name: {idn[1]} not equal {name} and sn: {idn[2]} not equal {sn} {bcolors.FAIL}')
                return None
        else:
            return instr
        
    

def switch_on(instrument,channel,voltage,current):

    set_output(instrument,0)
    #reset(instrument)
    select_channel(instrument,channel)
    set_voltage(instrument, voltage)
    set_current(instrument, current)
    set_output(instrument,1)

def switch_off(instrument):

    set_output(instrument,0)

def set_output(instrument,state):
    actual_state = instrument.query_int('OUTPut?')
    if actual_state != state:
        instrument.write(f'OUTPut {state}')
        print(f'{bcolors.OKGREEN}output switched to {state}{bcolors.ENDC}')
    elif actual_state == state:
        print(f'{bcolors.OKGREEN}output already {state}{bcolors.ENDC}')

def reset(instrument):
    instrument.write(f'*RST')
    print(f'{bcolors.OKGREEN}reset instrument{bcolors.ENDC}')
    
def set_voltage(instrument, voltage):
    actual_voltage = instrument.query_float('VOLTage?')
    if actual_voltage != voltage:
        print(f'{bcolors.OKCYAN}actual voltage is {actual_voltage} {bcolors.ENDC}')
        instrument.write(f'VOLTage {voltage}')
    actual_voltage = instrument.query_float('VOLTage?')
    print(f'{bcolors.OKCYAN}voltage: {actual_voltage} is set{bcolors.ENDC}')

def set_current(instrument, current):
    actual_current = instrument.query_float('CURRent?')
    if actual_current != current:
        print(f'{bcolors.OKCYAN}actual current is {actual_current} {bcolors.ENDC}')
        instrument.write(f'CURRent {current}')
    actual_current = instrument.query_float('CURRent?')
    print(f'{bcolors.OKCYAN}current: {actual_current} is set{bcolors.ENDC}')
        
def select_channel(instrument,channel):
    instrument.write(f'INSTrument OUT{channel}')
    print(f'{bcolors.OKCYAN}channel: {channel} is set{bcolors.ENDC}')

def main():

    args = parse_arguments()
    s_on = args.on
    s_off = args.off
    s_reset = args.reset
    voltage = args.voltage
    current = args.current
    channel = args.channel
    port = args.port
    
    verify_instrument = False
    name = os.getenv("POWER_SUPPLY_NAME")
    sn = os.getenv("POWER_SUPPLY_SN")
    if (name and sn):
        verify_instrument = True
        print(f'{bcolors.OKCYAN}instrument name and sn from env is set: {name} {sn}{bcolors.ENDC}')

    if(not port):
        port = os.getenv("POWER_SUPPLY_PORT")
        print(f'{bcolors.OKCYAN}instrument port from env is set: {port}{bcolors.ENDC}')

    if(not channel):
        channel = os.getenv("POWER_SUPPLY_CHANNEL")
        print(f'{bcolors.OKCYAN}channel from env is set: {channel}{bcolors.ENDC}')

    # basename = Path(__file__).stem
    # config = get_config(f'{basename}.cfg')

    # port = config["DEFAULT"]["VISAport"]
    # instr_id = config["DEFAULT"]["InstrumentID"]
    # voltage = config["DEFAULT"]["Voltage"]
    # current = config["DEFAULT"]["Current"]
    # channel = config["DEFAULT"]["Channel"]

    # instr = connect_to_instrument(port, instr_id)
    # switch_on(instr,channel,voltage,current)
    # switch_off(instr)

    if(s_reset):
        instr = connect_to_instrument(port, name, sn, verify_instrument)
        reset(instr)
    if(s_on and not s_off):
        instr = connect_to_instrument(port, name, sn, verify_instrument)
        switch_on(instr,channel,voltage,current)
    elif(not s_on and s_off):
        instr = connect_to_instrument(port, name, sn, verify_instrument)
        switch_off(instr)
    else:
        print(f'{bcolors.FAIL}Error: no parameter --on or --off given{bcolors.ENDC}')

    #instr_list = RsInstrument.list_resources("?*")
    #print(instr_list)

main()