#!/usr/bin/env python3

import os
import time
from subprocess import Popen
from time import monotonic as timer

batcmd = 'HiTest\HiTest.exe'
dir_path = 'finished'
timeout = 5

start = timer()
process = Popen(batcmd)
test_running = True
while(process.poll() is None): 
	
	if(os.path.exists(dir_path)):
		print(f'found {dir_path}')
		print(f'terminate {batcmd}')
		process.terminate()
		time.sleep(1)	
	else:
		print(f'check for {dir_path}')
		print(f'sleep {timeout}')
		time.sleep(timeout)
		
print('Elapsed seconds: {:.2f}'.format(timer() - start))