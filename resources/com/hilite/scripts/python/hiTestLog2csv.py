import argparse
from pathlib import Path
import sqlite3
import csv
from prettytable import from_csv

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def parse_arguments():
   parser = argparse.ArgumentParser(description='Export HiTest db log to csv')
   parser.add_argument('-d','--db',help='path to sqlite file', required=True, type=Path)
   parser.add_argument('-c','--csv',help='path for csv file', required=True, type=Path)

   return parser.parse_args()

def main():

    args = parse_arguments()

    db_file = args.db
    csv_file_path = args.csv

    # dbfile = Path('Tests/system_tests/HiTest/data/Log.db')
    # logfile = Path('Tests/system_tests/HiTestLog.csv')

    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()
    cursor.execute("select DATETIME(Date, 'unixepoch', 'localtime') as DateTime, * from Log;")

    with open(csv_file_path, 'w',newline='') as csv_file: 
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow([i[0] for i in cursor.description]) 
        csv_writer.writerows(cursor)
    conn.close()

    with open(csv_file_path) as table_file: 
        tab = from_csv(table_file)
        tab.align = "l"
        tab.fields=["DateTime", "Code", "Message", "Source"]
        tab._max_width = {"Message" : 100, "Source" : 50}
        print(tab)

main()

